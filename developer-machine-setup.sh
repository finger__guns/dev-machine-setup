#!/usr/bin/env bash
# This script will be used to setup a developers machine.
# Theres a few things needed!
# Homebrew the osx package manager.
# NVM will replace node as the default here,
# python3 as a dependency mostly.
# heroku and aws as server managment type things.

# Nice little bash things.
set -u
set -e
set -x

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

# Pretty little colours
RED=$'\e[1;31m'
GREEN=$'\e[1;32m'
YELLOW=$'\e[1;33m'
BLUE=$'\e[1;34m'
MAGENTA=$'\e[1;35m'
CYAN=$'\e[1;36m'
END=$'\e[0m'

function install_brew {
    printf "%s \n" "${YELLOW}Checking${END} if homebrew is installed🍺"
    if ! hash brew 2>/dev/null; then
        printf "%s \n" "${MAGENTA} Homebrew wasn't installed - lets fix that.${END} 🍻"
        printf "%s \n" "${YELLOW} INSTALLING ${END} "
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
        printf "%s \n" "${CYAN} Homebrew ${END} is now installed.🍻"
    else
        printf "%s \n" "${GREEN} You have brew installed already!${END}"
    fi
}

function install_node_version_manager {
    if ! hash nvm 2>/dev/null; then
        printf "%s \n" "${MAGENTA} NVM wasn't installed - lets fix that.${END} "
        printf "%s \n" "${YELLOW} INSTALLING ${END} "
        brew install nvm
        printf "%s \n" "${CYAN} NVM ${END} is now installed.🍻"
    else
        printf "%s \n" "${GREEN} You have NVM installed already!${END}"
    fi
}

function node_version_manager_configuration {
    printf "%s \n" "${YELLOW} INSTALLING Node lts version.${END} "
    nvm install --lts
    if [ -n "`$SHELL -c 'echo $ZSH_VERSION'`" ]; then
        echo "export NVM_DIR="$HOME/.nvm" NVM_HOMEBREW="/usr/local/opt/nvm/nvm.sh"
        [ -s "$NVM_HOMEBREW" ] && \. "$NVM_HOMEBREW"" >> ~/.zshenv
        echo "[ -x "$(command -v npm)" ] && export NODE_PATH=$NODE_PATH:`npm root -g`" >> ~/.zshenv
        printf "%s \n" "${GREEN} Wrote to zshenv file - nvm now configured${END}"
    elif [ -n "`$SHELL -c 'echo $BASH_VERSION'`" ]; then
        echo "export NVM_DIR="$HOME/.nvm" NVM_HOMEBREW="/usr/local/opt/nvm/nvm.sh"
        [ -s "$NVM_HOMEBREW" ] && \. "$NVM_HOMEBREW"" >> ~/.bash_profile
        echo "[ -x "$(command -v npm)" ] && export NODE_PATH=$NODE_PATH:`npm root -g`" >> ~/.bash_profile
        printf "%s \n" "${GREEN} Wrote to bash_profile file - nvm now configured${END}"
    else
        echo "export NVM_DIR="$HOME/.nvm" NVM_HOMEBREW="/usr/local/opt/nvm/nvm.sh"
        [ -s "$NVM_HOMEBREW" ] && \. "$NVM_HOMEBREW"" >> ~/.profile
        echo "[ -x "$(command -v npm)" ] && export NODE_PATH=$NODE_PATH:`npm root -g`" >> ~/.profile
        printf "%s \n" "${GREEN} Wrote to profile file - nvm now configured${END}"
    fi
}

function install_python {
    if ! hash python3 2>/dev/null; then
        printf "%s \n" "${MAGENTA} PYTHON wasn't installed - lets fix that.${END} 🐍"
        printf "%s \n" "${YELLOW} INSTALLING ${END} "
        brew install python
        printf "%s \n" "${GREEN} Python is installed! ${END}"
    else
        printf "%s \n" "${GREEN} Python was already installed.${END}"
    fi
}

function install_aws {
    if ! hash aws 2>/dev/null; then
        printf "%s \n" "${MAGENTA} AWScli isn't installed ${END}"
        printf "%s \n" "${YELLOW} INSTALLING ${END} "
        brew install awscli
        printf "%s \n" "${GREEN} AWScli installed! ${END}"
        printf "%s \n" "${CYAN} AWS STILL NEEDS CONFIGURATION! ${END}"
    else
        printf "%s \n" "${GREEN} AWScli was already installed.${END}"
    fi
}

function install_heroku {
    if ! hash heroku 2>/dev/null; then
        printf "%s \n" "${MAGENTA} Heroku isn't installed ${END}"
        printf "%s \n" "${YELLOW} Tapping ${END} "
        brew tap heroku/brew
        printf "%s \n" "${GREEN} Heroku tapped${END}"
        printf "%s \n" "${YELLOW} Installing. ${END} "
        brew install heroku
        printf "%s \n" "${GREEN} Heroku installed${END}"
        printf "%s \n" "${CYAN} HEROKU NEEDS CONFIGURATION ${END}"
    else
        printf "%s \n" "${GREEN} HEROKU was already installed.${END}"
    fi
}

function main {
    install_brew;
    install_python;
    install_node_version_manager;
    node_version_manager_configuration;
    install_aws;
    install_heroku;
}
main;
